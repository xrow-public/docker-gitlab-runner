FROM docker.io/gitlab/gitlab-runner:alpine-bleeding

COPY config-template.toml /

ENV TEMPLATE_CONFIG_FILE /config-template.toml
